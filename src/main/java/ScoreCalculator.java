import com.sun.tools.javac.code.Attribute;

import java.util.HashMap;

public class ScoreCalculator {

    String CHEAT = "0";
    String COOPERATE = "1";

//    enum moves{
//        CHEAT,
//        COOPERATE
//    }

    public HashMap getPlayerScores(String player1Move, String player2Move) {

        HashMap scores = new HashMap();
        if(player1Move==COOPERATE && player2Move==COOPERATE){
            scores.put("P1Score", "2");
            scores.put("P2Score", "2");
        }else if(player1Move==CHEAT && player2Move==CHEAT){
            scores.put("P1Score", "0");
            scores.put("P2Score", "0");
        }else if(player1Move==COOPERATE && player2Move==CHEAT){
            scores.put("P1Score", "-1");
            scores.put("P2Score", "3");
        }else if(player1Move==CHEAT && player2Move==COOPERATE){
            scores.put("P1Score","3");
            scores.put("P2Score","-1");
        }
        return scores;



    }
}
