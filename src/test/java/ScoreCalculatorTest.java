import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class ScoreCalculatorTest {

    ScoreCalculator sc = new ScoreCalculator();

    @Test
    public void shouldReturnNullInCaseOfInvalidMove(){
        HashMap response=new HashMap();
        Assert.assertEquals(response,sc.getPlayerScores("xgfcfgh","yhfgjf"));

    }

    @Test
    public void shouldReturn2and2WhenP1CooperateAndP2Corporate(){
        HashMap response=new HashMap();
        response.put("P1Score","2");
        response.put("P2Score","2");
        Assert.assertEquals(response,sc.getPlayerScores(sc.COOPERATE,sc.COOPERATE));
    }

    @Test
    public void shouldReturn0and0WhenP1CheatAndP2Cheat(){
        HashMap response=new HashMap();
        response.put("P1Score","0");
        response.put("P2Score","0");
        Assert.assertEquals(response,sc.getPlayerScores(sc.CHEAT,sc.CHEAT));
    }

    @Test
    public void shouldReturnNegative1and3WhenP1CooperateAndP2Cheat(){
        HashMap response=new HashMap();
        response.put("P1Score","-1");
        response.put("P2Score","3");
        Assert.assertEquals(response,sc.getPlayerScores(sc.COOPERATE,sc.CHEAT));
    }

    @Test
    public void shouldReturn3andNegative1WhenP1CheatAndP2Cooperate(){
        HashMap response=new HashMap();
        response.put("P1Score","3");
        response.put("P2Score","-1");
        Assert.assertEquals(response,sc.getPlayerScores(sc.CHEAT,sc.COOPERATE));
    }


}
